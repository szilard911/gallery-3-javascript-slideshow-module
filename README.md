### Gallery 3 javascript slideshow module ###
Replaces original Cooliris slideshow.

### Requirements: ###

Gallery 3 instance. (http://galleryproject.org/)
RSS module

### Usage: ###
Copy whole folder to 'your_gallery_install/modules' library and activate the module in the admin menu

NOTE: disable original slideshow first

### Contribution guidelines: ###

I've made just a few line of coding, all credit goes to
1. http://buildinternet.com/project/supersized/
2. Original Gallery 3 slideshow module

### Who do I talk to? ###

Sorry but I can't provide any support, absolutelly NO warranty (!) on the usage of the module.